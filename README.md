# Sequential id generator

Inkscape plugin to assign sequential id's to selected elements

## Installation

Put these files into `~/.config/inkscape/extensions/seqid`

## Usage

* Select the first element of the elements you want to assign sequential id's to
* Open Extensions -> Utilities -> Sequential id generator
* Configure prefix, postfix, and initial number
* The selected element will be given the requested id
* A settings box showing the next id will be shown. Feel free to move this box.
* Select another element and use Alt+Q to assign the next id

To reset all settings, remove the settings box and open the extension dialog again 

### Tips

Use 'Object'->'Object properties...' to show object id's

### Advanced

When selecting multiple elements, multiple sequential id's will be assigned based on
the 'direction' setting.

Select the settings panel and press Alt+Q to change the direction without
resetting the next id.
