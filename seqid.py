#!/usr/bin/env python3
#
# Copyright (C) 2022 Arnout Engelen, arnout@bzzt.net
#
# Partly based on the image_extract plugin:
#
# Copyright (C) 2005 Aaron Spike, aaron@ekips.org
#               2022 Jonathan Neuhauser, jonathan.neuhauser@outlook.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""
Assign sequential id's to selected elements
"""

import inkex

class SeqId(inkex.EffectExtension):
    """Assign sequential id's to selected objects"""

    def add_arguments(self, pars):
        pars.add_argument(
            "--prefix",
            type=str,
            help="Prefix",
            default="Object-",
        )
        pars.add_argument(
            "--postfix",
            type=str,
            help="Postfix",
            default="",
        )
        pars.add_argument(
            "-s",
            "--start",
            type=int,
            help="Start at",
            default=0,
        )
        pars.add_argument(
            "-n",
            "--step",
            type=int,
            help="Step",
            default=1,
        )
        pars.add_argument(
            "-d",
            "--direction",
            type=str,
            help="Direction",
            default="left-to-right",
        )
        pars.add_argument(
            "-f",
            "--force",
            type=inkex.Boolean,
            help="Force direction",
            default=False,
        )

    def __init__(self):
        super().__init__()

    def effect(self):
        self.errcount = 0

        prefix = self.options.prefix
        postfix = self.options.postfix
        step = self.options.step

        settingsElement = self.svg.getElementById('seqid-settings')
        if settingsElement == None:
            from inkex.elements import Rectangle, TextElement, Tspan, Group
            settingsElement = Group.new('seqid-settings', id = 'seqid-settings')
            height = 120
            rect = Rectangle.new(0,-height-10,120,height,
                style="stroke:#000000;fill:#ffffff;fill-opacity:1"
            )
            settingsElement.add(rect)
            headingStyle = "font-size: 12px"
            settingsElement.add(TextElement.new(Tspan.new("Sequential ids:", style=headingStyle), x=10, y=-height+10))
            fontStyle = "font-size: 8px"
            docFontStyle = "font-size: 6px; font-style: italic"
            hiddenStyle = "display: none"
            settingsElement.add(TextElement.new(Tspan.new("Prefix:", style=fontStyle), x=10, y=-height+20))
            settingsElement.add(TextElement.new(Tspan.new("Postfix:", style=fontStyle), x=10, y=-height+30))
            settingsElement.add(TextElement.new(Tspan.new("Step:", style=fontStyle), x=10, y=-height+40))
            settingsElement.add(TextElement.new(Tspan.new("Direction:", style=fontStyle), x=10, y=-height+50))
            settingsElement.add(TextElement.new(Tspan.new("Next id:", style=fontStyle), x=10, y=-height+60))
            settingsElement.add(TextElement.new(Tspan.new(prefix, style=fontStyle, id='seqid-prefix'), x=50, y=-height+20))
            settingsElement.add(TextElement.new(Tspan.new(postfix, style=fontStyle, id='seqid-postfix'), x=50, y=-height+30))
            settingsElement.add(TextElement.new(Tspan.new(str(step), style=fontStyle, id='seqid-step'), x=50, y=-height+40))
            settingsElement.add(TextElement.new(Tspan.new(str(self.options.start), style=hiddenStyle, id='seqid-start')))
            settingsElement.add(TextElement.new(Tspan.new(self.options.direction, style=fontStyle, id='seqid-direction'), x=50, y=-height+50))
            settingsElement.add(TextElement.new(Tspan.new("", style=fontStyle, id='seqid-next'), x=50, y=-height+60))
            settingsElement.add(TextElement.new(Tspan.new("Alt-Q: to apply the next id(s)", style=docFontStyle), x=15, y=-height+80))
            settingsElement.add(TextElement.new(Tspan.new("to the selected object(s)", style=docFontStyle), x=20, y=-height+90))
            settingsElement.add(TextElement.new(Tspan.new("Remove this block to reset", style=docFontStyle), x=15, y=-height+100))
            self.svg.add(settingsElement)

        elems = self.svg.selection
        nextNum = int(self.svg.getElementById('seqid-start').text)
        nextId = prefix + str(nextNum) + postfix

        if self.options.force:
            direction = self.options.direction
        else:
            direction = self.svg.getElementById('seqid-direction').text or self.options.direction

        if (direction == 'left-to-right'):
            sortedElems = sorted(elems, key=lambda e: e.path.bounding_box().center.x)
        elif (direction == 'top-to-bottom'):
            sortedElems = sorted(elems, key=lambda e: e.path.bounding_box().center.y)
        elif (direction == 'right-to-left'):
            sortedElems = sorted(elems, key=lambda e: -e.path.bounding_box().center.x)
        else:
            sortedElems = sorted(elems, key=lambda e: -e.path.bounding_box().center.y)

        if ((not self.options.force) and len(elems) == 1 and elems[0].get('id') == 'seqid-settings'):
            previousDirection = self.svg.getElementById('seqid-direction').text or direction
            if (previousDirection == 'left-to-right'):
                direction = 'top-to-bottom'
            elif (previousDirection == 'top-to-bottom'):
                direction = 'right-to-left'
            elif (previousDirection == 'right-to-left'):
                direction = 'bottom-to-top'
            else:
                direction = 'left-to-right'

        for elem in sortedElems:
            if (elem.get('id') != 'seqid-settings'):
                elem.set('id', nextId)
                nextNum = nextNum + step
                nextId = prefix + str(nextNum) + postfix

        self.svg.getElementById('seqid-start').text = str(nextNum)
        self.svg.getElementById('seqid-next').text = nextId
        self.svg.getElementById('seqid-prefix').text = prefix
        self.svg.getElementById('seqid-postfix').text = postfix
        self.svg.getElementById('seqid-step').text = str(step)
        self.svg.getElementById('seqid-direction').text = direction


if __name__ == "__main__":
    SeqId().run()
